# -*- coding: utf-8 -*-
import configurations
from sign import _get_sign
from flask import render_template
import json
import requests
import datetime
import pickle
import os


def rub_api(amount, currency, shop_invoice_id, payway, description):

    request_ = {
        "shop_id": configurations.shop_id,
        "amount": amount,
        "payway": payway,
        "currency": currency,
        'shop_invoice_id': shop_invoice_id
    }

    keys_required = ("shop_id", "amount", "currency", "shop_invoice_id")
    sign_key = _get_sign(request_, keys_required, configurations.secret_key)

    query = {
        "shop_invoice_id": shop_invoice_id,
        "sign": sign_key,
        "currency": currency,
        "amount": amount,
        "shop_id": configurations.shop_id,
        "description": description
    }

    return render_template(template_name_or_list='tip_form.html', form_data=query)


def uah_api(amount, currency, shop_invoice_id, payway, description):

    request_ = {
            "shop_id": configurations.shop_id,
            "amount": amount,
            "payway": payway,
            "currency": currency,
            'shop_invoice_id': shop_invoice_id
    }

    keys_required = ("shop_id", "amount", "currency", "payway", "shop_invoice_id")
    # shop_invoice_id - almost required paramentr, controlled by our side for non duplicating transactions
    sign_key = _get_sign(request_, keys_required, configurations.secret_key)

    query = {
        "description": description,
        "payway": payway,
        "shop_invoice_id": shop_invoice_id,
        "sign": sign_key,
        "currency": currency,
        "amount": amount,
        "shop_id": configurations.shop_id
    }
    query_json = json.dumps(query)
    headers = {"Content-Type":  "application/json"}
    json_response = requests.post(configurations.invoice_api_url_uah, data=query_json, headers=headers)
    json_from_trio_uah = json.loads(json_response.text, encoding='utf-8')
    if json_from_trio_uah["message"]!='Ok' and json_from_trio_uah["result"]!='ok':
        return "Wrong parameters to invoice_api_url_uah"
    data = json_from_trio_uah['data']
    print data['data']
    return render_template(template_name_or_list='transaction_form.html', form_data=data)


def logging(currency, amount, description):
    properties = (currency, amount, description)
    with open('logs.txt', 'a') as log:
            log.write('{} --- {}\n'.format(datetime.datetime.now().strftime(configurations.datetimeformat), properties))


def get_lst_id():
    path = os.getcwd()
    if os.path.isfile('{}/db.pkl'.format(path)):
        with open('db.pkl', 'rb') as db_file:
            db = pickle.load(db_file)
            return int(db['shop_invoice_id']) + 1

    else:
        with open('db.pkl', 'wb+') as db_file:
            db = {'shop_invoice_id': 1}
            pickle.dump(db, db_file, pickle.HIGHEST_PROTOCOL)
            return '1'


def set_count_id(shop_invoice_id):
    with open('db.pkl', 'wb+') as db_file:
        pickle.dump({'shop_invoice_id': shop_invoice_id}, db_file, pickle.HIGHEST_PROTOCOL)