import requests
import configurations
import json


class Adminka(object):

    post_data = {
        'key': 'secrettokenkey',
        'user': 'koss',
        'source': 'heroku'
    }

    def __init__(self, type):
        self.type = type
        self.resp = []

    def makeRequest(self):
        post = dict(Adminka.post_data)
        post['type'] = self.type
        request = requests.request(method='post', url=configurations.admin_api_url, data=post)
        self.resp.append(request)
        return request.status_code

    def getData(self):
        return json.loads(self.resp[0].content)


if __name__ == "__main__":

    req = Adminka('todevs')
    print req.makeRequest()
