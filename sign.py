import hashlib


def _get_sign(request, keys_required, secret):
    keys_sorted = sorted(keys_required)
    string_to_sign = ":".join([request[k].encode("utf8") for k in keys_sorted]) + secret
    sign = hashlib.md5(string_to_sign).hexdigest()
    return sign

