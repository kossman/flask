# -*- coding: utf-8 -*-
from flask import Flask
from flask import request
from flask import render_template
import configurations
import api
import adminka


app = Flask(__name__)


@app.route('/')         # start page
def render_invoice():
    return render_template(template_name_or_list='create_invoice.html')


@app.route('/invoice', methods=['POST'])
def create_invoice():
    if request.method != 'POST':
        return "WRONG REQUEST TYPE"
    amount = str(request.form.get('amount'))
    payway = str(request.form.get('payway'))
    description = request.form.get('description')
    shop_invoice_id = str(api.get_lst_id())     # take last shop_invoice_id for next transaction
    api.set_count_id(shop_invoice_id)           # set new value to db.pkl file

    if payway == 'rub':
        currency = configurations.rub
        api.logging('RUB', amount, description)
        return api.rub_api(amount, currency, shop_invoice_id, payway, description)
    elif payway == 'w1_uah':
        currency = configurations.uah
        api.logging('UAH', amount, description)
        return api.uah_api(amount, currency, shop_invoice_id, payway, description)
    else:
        return "Bad parameters for request"


@app.route('/logs')         # for view logs
def logs():
    with open('logs.txt') as log:
        return render_template(template_name_or_list='logs.html', items=log.readlines())


@app.route('/logs/trun')
def log_truncate():
    with open('logs.txt', 'wb') as log:
        log.truncate()
        return "Log file was truncate"


@app.route('/todevs')
def todevs():
    req = adminka.Adminka('todevs')
    result = req.makeRequest()
    if result == 200:
        return render_template(template_name_or_list='onbootstrap/adminka_content.html',
                               content={'messages': req.getData()['data']})
    else:
        return 'Wrong HTTP code responce'

@app.route('/fromsite')
def fromsite():
    req = adminka.Adminka('fromsite')
    result = req.makeRequest()
    if result == 200:
        return render_template(template_name_or_list='onbootstrap/adminka_content.html',
                               content={'messages': req.getData()['data']})
    else:
        return 'Wrong HTTP code responce'

@app.route('/complaints')
def complaints():
    req = adminka.Adminka('complaints')
    result = req.makeRequest()
    if result == 200:
        return render_template(template_name_or_list='onbootstrap/adminka_content.html',
                               content={'messages': req.getData()['data']})
    else:
        return 'Wrong HTTP code responce'


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
